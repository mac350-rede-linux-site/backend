package com.example.plugins

import com.example.models.io.InputPublicKey
import com.example.models.io.InputUserInfo
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.plugins.requestvalidation.*
import io.ktor.server.plugins.statuspages.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import java.util.regex.Pattern

fun Application.configureSerialization() {
    install(ContentNegotiation) {
        json()
    }

    install(RequestValidation) {
        validate<InputUserInfo> { info ->
            validateUserInfo(info)
                ?.let { ValidationResult.Invalid(it) }
                ?: ValidationResult.Valid
        }

        validate<InputPublicKey> { key ->
            validatePublicKey(key)
                ?.let { ValidationResult.Invalid(it) }
                ?: ValidationResult.Valid
        }
    }

    install(StatusPages) {
        exception<RequestValidationException> { call, cause ->

            call.respondText(cause.message ?: "Bad Input", status = HttpStatusCode.BadRequest)

        }
    }
}

val validGroups = listOf("bcc", "be", "bmac", "lic", "licn")

fun validateUserInfo(info: InputUserInfo): String? {

    val validations: List<Pair<String, (InputUserInfo) -> Boolean>> = listOf(
        "Nome de ususário é muito grande (máximo é 16 caracteres)" to { it.username.length > 16 },
        "Username vazio" to { it.username.isEmpty() },
        "Nome de usuário só pode ter letra" to { !Regex("[a-z]+").matches(it.username) },
        "Grupo inválido" to { it.group !in validGroups },
        "O seu nome completo é grande demais (limite de 255 caracteres)" to { it.fullName.length > 255 },
        "Nome vazio" to { it.fullName.isEmpty() }
    )

    return validations.find { it.second(info) }?.first
}

// isto é uma HEURÍSTICA para ajudar a detectar chaves SSH zoadas,
// mas vai deixar passar chaves que não funcionam; se o usuário colocar algo zoado,
// que envie e-mail para nós
// esta heurística é inspirada na específicação da man page do sshd,
// seção AUTHORIZED_KEYS, OpenSSH 9.6, OpenSSL 3.0.13


fun validatePublicKey(info: InputPublicKey): String? {
    val key = info.key

    val lines = key.split("\n")

    if (lines.any { it.length > 8 * 1024 }) {
        return "Chave muito grande. Se estiver usando RSA, use Ed25519 a partir deste ponto."
    }

    if (lines.any { line -> ! someRelevantKeywords.any { it in line } }) {
        return "Algorítmo desconhecido"
    }

    return null
}

private val someRelevantKeywords =
    listOf(
        "sk-ecdsa-sha2-nistp256@openssh.com",
        "ecdsa-sha2-nistp256",
        "ecdsa-sha2-nistp384",
        "ecdsa-sha2-nistp521",
        "sk-ssh-ed25519@openssh.com",
        "ssh-ed25519",
        "ssh-dss",
        "ssh-rsa",
    )
