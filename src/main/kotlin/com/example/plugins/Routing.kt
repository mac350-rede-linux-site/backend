package com.example.plugins

import com.example.content.*
import com.example.models.io.InputPublicKey
import com.example.models.io.InputUserInfo
import com.example.models.io.OutputUserInfo
import com.example.models.io.PasswordReset
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.util.pipeline.*

fun Application.configureRouting() {
    routing {
        route("/account") {
            get("/info") {
                withQueryNusp { nusp ->
                    connectToKeyDatabase().use { keyDatabase ->
                        connectToDatabase().use { db ->
                            val user = db.getUserFromNusp(nusp)

                            if (user == null) {
                                call.respondText(
                                    "UNKNOWN NUSP",
                                    status = HttpStatusCode.NotFound
                                )
                            } else {
                                val out = OutputUserInfo.from(user, keyDatabase.hasKey(user))

                                call.respond(out)
                            }
                        }
                    }
                }
            }

            post("/create") {
                withQueryNusp { nusp ->
                    connectToDatabase().use { db ->
                        val user = call.receive<InputUserInfo>()

                        val ok = db.createUser(nusp, user)

                        if (!ok) {
                            call.respondText("Usuário já tem conta", status = HttpStatusCode.Conflict)
                            return@use
                        }

                        call.respond(HttpStatusCode.OK)
                    }
                }
            }

            get("/key") {
                withQueryNusp { nusp ->
                    connectToDatabase().use { db ->
                        val info = db.getUserFromNusp(nusp)

                        if (info == null) {
                            call.respond(HttpStatusCode.NotFound)
                            return@use
                        }

                        connectToKeyDatabase().use { keydb ->
                            val key = keydb.getKey(info)

                            if (key == null) {
                                call.respond(HttpStatusCode.NotFound)
                            }
                            else {
                                call.respondText(key, status = HttpStatusCode.OK)
                            }
                        }
                    }
                }
            }

            post("/key") {
                withQueryNusp { nusp ->
                    connectToDatabase().use { db ->
                        val info = db.getUserFromNusp(nusp)

                        if (info == null) {
                            call.respond(HttpStatusCode.NotFound)
                            return@use
                        }

                        val data = call.receive<InputPublicKey>()

                        connectToKeyDatabase().use { keys ->

                            if (data.key.trim().isEmpty()) {
                                keys.deleteKey(info)
                            }
                            else {
                                keys.putKey(info, data.key)
                            }
                        }

                        call.respond(HttpStatusCode.OK)
                    }
                }
            }

            post("/remote") {
                withQueryNusp { nusp ->
                    connectToDatabase().use { db ->
                        val data = call.receive<Boolean>()

                        db.setRemoteFromNusp(nusp, data)

                        call.respond(HttpStatusCode.OK)
                    }
                }
            }

            delete("/key") {
                withQueryNusp { nusp ->
                    connectToDatabase().use { db ->
                        val info = db.getUserFromNusp(nusp)

                        if (info == null) {
                            call.respond(HttpStatusCode.NotFound)
                            return@use
                        }

                        connectToKeyDatabase().use { keys ->
                            keys.deleteKey(info)
                        }

                        call.respond(HttpStatusCode.OK)
                    }
                }
            }

            post("/reset_password") {
                withQueryNusp { nusp ->
                    connectToDatabase().use { db ->

                        val info = db.getUserFromNusp(nusp)

                        if (info == null) {
                            call.respond(HttpStatusCode.NotFound)
                            return@use
                        }

                        val password = call.receive<PasswordReset>()

                        if (password.newPassword.isEmpty()) {
                            call.respondText("SENHA RUIM.", status = HttpStatusCode.BadRequest)
                            return@use
                        }

                        db.setPassword(nusp, password.newPassword)

                        call.respond(HttpStatusCode.OK)
                    }
                }
            }
        }
    }
}

fun connectToDatabase(): DatabaseAccess = LdapDatabaseAccess()
fun connectToKeyDatabase(): PublicKeyAccess = DirectPublicKeyAccess()


suspend fun <T> PipelineContext<Unit, ApplicationCall>.withQueryNusp(lambda: suspend PipelineContext<Unit, ApplicationCall>.(Int) -> T) {
    val nuspString = call.request.queryParameters["nusp"]
    val nusp = nuspString?.toIntOrNull()

    if (nuspString == null || nusp == null) {
        call.respondText(
            "UNKNOWN NUSP",
            status = HttpStatusCode.BadRequest
        )
    }
    else {
        lambda(nusp)
    }
}
