package com.example.content

import com.example.models.io.InputUserInfo
import com.example.models.io.UserInfo
import java.io.File
import java.sql.DriverManager
import java.sql.SQLException

interface DatabaseAccess : AutoCloseable {
    fun getUserFromNusp(nusp: Int): UserInfo?
    fun setRemoteFromNusp(nusp: Int, remoteEnabled: Boolean)
    fun createUser(nusp: Int, userInfo: InputUserInfo): Boolean
    fun setPassword(nusp: Int, password: String)
}

const val DATABASE_DIRECTORY = "./data/"
const val DATABASE_LOCATION = "${DATABASE_DIRECTORY}/database"

class SqliteDatabaseAccess : DatabaseAccess {
    private val connection = connect()

    private fun connect(): java.sql.Connection {
        File(DATABASE_DIRECTORY).mkdir()
        val exists = File(DATABASE_LOCATION).exists()
        val connection = DriverManager.getConnection("jdbc:sqlite:${DATABASE_LOCATION}")

        if (!exists) {
            initialize(connection)
        }

        return connection
    }

    private fun initialize(connection: java.sql.Connection) {
        val commands = listOf("""
            create table if not exists UserData(
                nusp integer primary key,
                user_name text unique,
                user_group text,
                full_name text,
                email text,
                password text,
                remote_enabled integer,
                join_date integer
            );
        """.trimIndent())

        for (command in commands) {
            connection.prepareStatement(command).use { it.execute() }
        }
    }

    override fun getUserFromNusp(nusp: Int): UserInfo? {
        val statement = connection.prepareStatement("select * from UserData where UserData.nusp = ?")

        statement.setInt(1, nusp)

        val result = statement.executeQuery()

        if (result.next()) {
            return UserInfo(
                username = result.getString("user_name"),
                nusp = result.getInt("nusp"),
                group = result.getString("user_group"),
                fullName = result.getString("full_name"),
                hasRemote = result.getInt("remote_enabled") != 0,
                email = result.getString("email"),
                joinDate = result.getLong("join_date"),
            )
        }

        return null
    }

    override fun setRemoteFromNusp(nusp: Int, remoteEnabled: Boolean) {
        connection.autoCommit = true

        val statement = connection.prepareStatement("update UserData set remote_enabled = ? where nusp = ?")

        statement.setInt(1, if (remoteEnabled) { 1 } else { 0 })
        statement.setInt(2, nusp)

        statement.executeUpdate()
    }

    override fun createUser(nusp: Int, userInfo: InputUserInfo): Boolean {
        connection.autoCommit = false

        try {
            if (nuspExists(nusp) || usernameExists(userInfo.username)) {
                return false
            }

            val statement = connection.prepareStatement(
                """
                    insert into UserData (
                    nusp,
                    user_name, 
                    user_group, 
                    full_name, 
                    email,
                    password,
                    join_date,
                    remote_enabled
                    ) values (?, ?, ?, ?, ?, ?, ?, false);
                    """.trimIndent()
            )

            statement.setInt(1, nusp)
            statement.setString(2, userInfo.username)
            statement.setString(3, userInfo.group)
            statement.setString(4, userInfo.fullName)
            statement.setString(5, userInfo.email)
            statement.setString(6, computePasswordHash(userInfo.password))
            statement.setLong(7, java.util.Date().time / 1000)

            statement.executeUpdate()
            connection.commit()
        } catch (issue: SQLException) {
            connection.rollback()
            throw issue
        }

        return true
    }

    override fun setPassword(nusp: Int, password: String) {
        val hash = computePasswordHash(password)
        val statement = connection.prepareStatement("update UserData set password = ? where nusp = ?")

        statement.setString(1, hash)
        statement.setInt(2, nusp)

        statement.executeUpdate()
    }

    private fun usernameExists(username: String): Boolean =
        connection.prepareStatement("select user_name from UserData where user_name = ?").use { statement ->
            statement.setString(1, username)
            val existsResult = statement.executeQuery()
            existsResult.next()
        }

    private fun nuspExists(nusp: Int): Boolean =
        connection.prepareStatement("select user_name from UserData where nusp = ?").use { statement ->
            statement.setInt(1, nusp)
            val existsResult = statement.executeQuery()
            existsResult.next()
        }

    override fun close() {
        connection.close()
    }

}