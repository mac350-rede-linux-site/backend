package com.example.content

import java.security.SecureRandom
import java.security.spec.KeySpec
import java.util.Base64
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec

fun computePasswordHash(input: String): String {
    val random = SecureRandom()
    val salt = ByteArray(16)
    random.nextBytes(salt)

    val hash = computeHash(input, salt)

    val encoder = Base64.getEncoder()
    return encoder.encodeToString(salt) + ":" + encoder.encodeToString(hash)
}

private fun computeHash(input: String, salt: ByteArray): ByteArray {
    val spec: KeySpec = PBEKeySpec(input.toCharArray(), salt, 1 shl 20, 256)
    val factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256")

    return factory.generateSecret(spec).encoded
}

fun hashMatches(hash: String, input: String): Boolean {
    if (':' !in hash) {
        throw IllegalArgumentException("Invalid hash")
    }

    val saltString = hash.takeWhile { it != ':' }

    val decoder = Base64.getDecoder()

    val salt = decoder.decode(saltString)
    val originalHash = decoder.decode(hash.dropWhile { it != ':' }.drop(1))

    val computedHash = computeHash(input, salt)

    return originalHash.contentEquals(computedHash)
}