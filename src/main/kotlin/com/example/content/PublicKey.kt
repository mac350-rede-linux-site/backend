package com.example.content

import com.example.models.io.UserInfo
import java.io.File
import java.io.FileOutputStream
import java.nio.file.Paths

interface PublicKeyAccess : AutoCloseable {
    fun putKey(info: UserInfo, key: String)
    fun hasKey(info: UserInfo): Boolean
    fun deleteKey(info: UserInfo)
    fun getKey(info: UserInfo): String?
}

private const val PUBLIC_KEY_PATH = "./public-keys"

class DirectPublicKeyAccess : PublicKeyAccess {

    override fun putKey(info: UserInfo, key: String) {
        val file = getPublicKeyPath(info)

        val stream = FileOutputStream(file)

        stream.write(key.toByteArray(Charsets.UTF_8))
    }

    private fun getPublicKeyPath(info: UserInfo): File {
        File(PUBLIC_KEY_PATH).mkdirs()
        return Paths.get(PUBLIC_KEY_PATH, info.username).toFile()
    }

    override fun hasKey(info: UserInfo): Boolean = getPublicKeyPath(info).isFile

    override fun deleteKey(info: UserInfo) {
        getPublicKeyPath(info).delete()
    }

    override fun getKey(info: UserInfo): String? {

        val file = getPublicKeyPath(info)

        return if (file.isFile) {
            file.readText(Charsets.UTF_8)
        } else { null }
    }

    override fun close() {
        // unused, but we keep it to make it more uniform/pretty
    }
}
