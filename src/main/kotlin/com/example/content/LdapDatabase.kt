package com.example.content

import com.example.models.io.InputUserInfo
import com.example.models.io.UserInfo
import com.unboundid.ldap.sdk.Filter.createANDFilter
import com.unboundid.ldap.sdk.Filter.createEqualityFilter
import com.unboundid.ldap.sdk.GSSAPIBindRequest
import com.unboundid.ldap.sdk.GSSAPIBindRequestProperties
import com.unboundid.ldap.sdk.LDAPConnection
import com.unboundid.ldap.sdk.Modification
import com.unboundid.ldap.sdk.ModificationType
import com.unboundid.ldap.sdk.SearchScope
import io.github.cdimascio.dotenv.dotenv


/**
 * O lendário e famigerado LDAP.
 *
 * ### INTRODUÇÂO
 *
 * De acordo com minha (Renan Ribeiro) interpretação LDAP, é um banco de dados NoSQL bem velho, que serve
 * para diversos propóositos, mas é especialmente útil para guardar informações de usuários e grupos,
 * pois diversas ferramentas tem suporte a autorização por LDAP, o que não é o caso quando se usa um banco
 * de dados mais tradicional, como PostgreSQL.
 * Por isso, nós guardamos atributos dos usuários e grupos neste sistema de banco de dados.
 *
 * O LDAP guarda as informações num sistema de árvore, como se fosse um sistema de arquivos, com vários diretórios (pastas).
 * Porém LDAP tem algumas diferenças em relação a sistemas de arquivos tradicionais:
 * - Todas as pastas e arquivos tem um nome da forma `a=b`, onde `a` e `b` são strings
 * - Caminhos são separados por vírgulas
 * - Caminhos são escritos da direita pra esquerda
 * - Caminhos completos normalmente são chamados de 'dn'
 *
 * Por exemplo, os usuários no ldap normalmente ficam na pasta de caminho/dn "ou=People,dc=linux,dc=ime,dc=usp,dc=br".
 *
 * Os 'arquivos' no ldap são como objetos, que possuem classes (normalmente chamada de objectClass) e atributos.
 *
 * ### USO
 *
 * Em particular, para poder interagir com o LDAP da rede, recomendo o uso da ferramenta `shelldap`, que permite
 * a interação com o LDAP como se fosse um shell normal.
 *
 * ```
 * shelldap --server ldap
 * # ou --server 127.0.0.1 --port 4000 se estiver acessando por túnel SSH
 * ```
 *
 * Aqui, você pode usar alguns comandos linux comuns, como `pwd`, `ls`, `cd`, e `cat`.
 *
 * Como muitos outros bancos de dados, o LDAP possui um sistema simples de autenticação e autorização, para
 * impedir que certas queries sejam feitas por certos serviços. Em geral, essas contas no ldap
 * são chamadas de binds, e toda conta possui um dn (caminho completo) e uma senha.
 *
 * É possível interagir com o servidor ldap sem passar um bind, mas algumas operações não serão permitidas,
 * e alguns campos (como o número usp dos usuários) não estarão visíveis.
 *
 * Para passar um bind para o shelldap, use as flags --binddn <dn do bind da conta> --promptpass
 *
 * A rede linux atualmente possui um bind chamado "providentia" simples para o ldap,
 * porém ele só tem acesso read-only aos diretórios da rede.
 *
 * E é aí que entra o kerberos.
 *
 * #### KERBEROS
 *
 * A Rede utiliza kerberos para realizar a autenticação com ldap. O servidor OpenLDAP tem suporte a um protocolo chamado
 * SASL, que permite diversos tipos de autenticação. Em particular, ele permite autenticação por outro procolo
 * chamado GSSAPI que, na prática, é um protcolo usado exclusivamente para se conectar com o Kerberos.
 *
 * Isso significa que é possível utilizar credenciais kerberos para se autenticar com o kerberos,
 * e é isso que esta implementação faz.
 */

class LdapDatabaseAccess : DatabaseAccess {

    val connection = run {
        val env = dotenv {
            ignoreIfMissing = true
        }

        // Aqui, definimos como a library de LDAP deve se comunicar com o servidor kerberos

        // principal (username) da pessoa que queremos logar, no caso o usuário megazord/admin
        val properties = GSSAPIBindRequestProperties("megazord/admin", "")

        // Por padrão, podemos nos autenticar utilizando a senha do usuário admin do kerberos.
        // Porém, ninguém sabe a senha do megazord, mas temos um arquivo chamado mega.keytab que é equivalente
        // a uma senha.
        properties.setUseKeyTab(true)
        properties.keyTabPath = env["RL_KERBEROS_USER_KEYTAB_PATH"]

        properties.kdcAddress = env["RL_KERBEROS_KDC_HOST"]
        properties.realm = "LINUX.IME.USP.BR"

        // quando o client ldap faz uma conexão com o servidor kerberos, ele avisa o kerberos qual serviço ele planeja
        // usar, no caso, o serviço é ldap/catuaba.linux.ime.usp.br (no caso, o ldap/ é inserido automaticamente)
        properties.saslClientServerName = env["RL_KERBEROS_LDAP_SERVICE_NAME"]
        properties.setUseKeyTab(true)

        val bindRequest = GSSAPIBindRequest(properties)

        val host = env["RL_LDAP_HOST"]
        val port = env["RL_LDAP_PORT"].toInt()
        val connection = LDAPConnection()
        connection.connect(host, port)
        connection.bind(bindRequest)
        connection
    }

    override fun getUserFromNusp(nusp: Int): UserInfo? {
        // nusp é um inteiro então tudo bem fazer interpolação

        val searchResult = connection.search("ou=People,dc=linux,dc=ime,dc=usp,dc=br", SearchScope.SUB, "nusp=${nusp}")

        val entries = searchResult.searchEntries

        if (entries.isEmpty()) {
            return null
        }

        val entry = entries[0]

        val groupId = entry.getAttributeValueAsInteger("gidNumber")

        return UserInfo(
            username = entry.getAttribute("uid").value,
            nusp = entry.getAttribute("nusp").value.toInt(),

            // o grupo da pessoa não fica salvo no objeto da pessoa, só o número do grupo fica
            // então temos que buscar o número do grupo na pasta de grupos.
            group = getUserGroupFromGroupId(groupId),

            fullName = entry.getAttribute("cn").value,

            // o mesmo vale para o remote da pessoa
            hasRemote = askIfUserIsRemote(entry.getAttributeValue("uid")),
            email = entry.getAttribute("mail").value,
            joinDate = 0, // TODO: add join date to ldap and get its value from LDAP
        )
    }


    private fun getUserGroupFromGroupId(id: Int): String {
        val searchResult =
            connection.search("ou=Group,dc=linux,dc=ime,dc=usp,dc=br", SearchScope.SUB, "gidNumber=${id}")

        val entries = searchResult.searchEntries

        if (entries.isEmpty()) {
            throw IllegalStateException("user group id does not exist 0~o")
        }

        val entry = entries.get(0)

        return entry.getAttribute("cn").value
    }


    // no nosso ldap, as informações de quais que são remote estão em um objeto só, em um campo memberUid
    private fun askIfUserIsRemote(username: String): Boolean {

        // Poderíamos ter usado uma string de filtro como "&(memberUid=${username})(cn=remote)",
        // mas isso poderia causar problemas, estilo SQL injection

        val filter = createANDFilter(
            createEqualityFilter("memberUid", username),
            createEqualityFilter("cn", "remote")
        )

        val searchResult = connection.search(
            "cn=remote,ou=Group,dc=linux,dc=ime,dc=usp,dc=br", SearchScope.SUB,
            filter
        )

        return searchResult.entryCount > 0
    }

    override fun setRemoteFromNusp(nusp: Int, remoteEnabled: Boolean) {
        val username = getUsernameFromNusp(nusp)

        val changes = if (remoteEnabled) {
            Modification(ModificationType.ADD, "memberUid", username)
        }
        else {
            Modification(ModificationType.DELETE, "memberUid", username)
        }

        connection.modify("cn=remote,ou=Group,dc=linux,dc=ime,dc=usp,dc=br", changes)
    }

    private fun getUsernameFromNusp(nusp: Int): String {
        val searchResult = connection.search("ou=People,dc=linux,dc=ime,dc=usp,dc=br", SearchScope.SUB, "nusp=${nusp}")

        val entries = searchResult.searchEntries

        if (entries.isEmpty()) {
            throw IllegalArgumentException("Unknown NUSP")
        }

        val entry = entries[0]

        return entry.getAttribute("uid").value
    }

    override fun createUser(nusp: Int, userInfo: InputUserInfo): Boolean {
        TODO("Not yet implemented")
    }

    override fun setPassword(nusp: Int, password: String) {
        TODO("Not yet implemented")
    }

    override fun close() {
        connection.close()
    }
}