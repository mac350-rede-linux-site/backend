package com.example.models.io

import kotlinx.serialization.Serializable

@Serializable
class InputPublicKey(
    val key: String
)
