package com.example.models.io

import kotlinx.serialization.Serializable

data class UserInfo(
    val username: String,
    val nusp: Int,

// TODO: usar tipo que melhor restringe o grupo.
    val group: String,

    val fullName: String,
    val hasRemote: Boolean,
    val email: String,
    val joinDate: Long
)

@Serializable
data class OutputUserInfo(
    val username: String,
    val nusp: Int,
    val group: String,
    val fullName: String,
    val hasRemote: Boolean,
    val email: String,
    val joinDate: Long,
    val hasKey: Boolean,
) {
    companion object {
        fun from(user: UserInfo, hasKey: Boolean): OutputUserInfo =
            OutputUserInfo(
                username = user.username,
                nusp = user.nusp,
                group = user.group,
                fullName = user.fullName,
                hasRemote = user.hasRemote,
                email = user.email,
                joinDate = user.joinDate,
                hasKey = hasKey
            )

    }
}

@Serializable
data class PasswordReset(
    val newPassword: String
)

@Serializable
data class InputUserInfo(
    val username: String,
    val email: String,
    val group: String,
    val fullName: String,
    val password: String
)
