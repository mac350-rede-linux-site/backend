package com.example

import com.example.plugins.*
import io.github.cdimascio.dotenv.dotenv
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*

fun main() {
    val env = dotenv {
        ignoreIfMissing = true
    }
    val port = env["RL_PORT"]?.let { Integer.parseInt(it) } ?: 8080
    val host = env["RL_HOST"] ?: "0.0.0.0"

    embeddedServer(Netty, port = port, host = host, module = Application::module)
        .start(wait = true)
}

fun Application.module() {
    configureRouting()
    configureSerialization()
}
