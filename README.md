
# Backend Novo da Rede Linux

- O que é esse projeto?

Este é um projeto que tenta implementar um backend novo para o site do serviço de contas da rede linux.

O backend antigo era feito em flask, e apesar de funcionar, ele é uma solução temporária (permanente), que
planejamos substituir.

Esta versão é implementada em kotlin, que é uma linguagem decente.
O desenvolvimento é realizado inteiramente utilizando a versão community da IDE, para evitar vendor lock
à versão Ultimate (mesmo que a USP nos dê acesso à versão ultimate da jetbrains).

## Execução

Para facilitar o desenvolvimento futuro, esse projeto providencia um `flake.nix` com as configurações
de desenvolvimento e build. Para usar isto, você precisará instalar [`nix`](https://nixos.org/) e
ligar [`flakes`](https://nixos.wiki/wiki/Flakes).

- Como eu posso desenvolver/modificar esse projeto?

É recomendado o uso do IntelliJ Idea Community Edition para desenvolver o projeto.
É possível usar o Ultimate Edition, mas é bom que evitemos acoplamento do projeto com
features que só podem ser utilizadas corretamente pelo ultimate. Por exemplo, os testes de API 
não são feitos com o client de APIs Ultimate Edition, mas são 
feitos com o client de API [bruno](https://www.usebruno.com/).

Os arquivos do client REST estão na pasta `bruno`. 

### LDAP

Para testar a integração com o ldap real da rede linux, estando fora da rede, recomendamos o uso de SSH tunelling:

``` 
ssh seu_usuario@linux.ime.usp.br -L 4000:ldap:389
```

Agora, a porta 4000 do seu computador redireciona para o servidor LDAP da Rede.

Você também precisará de um arquivo `.env` definir as variáveis de ambiente solicitadas pelo projeto
como porta de execução, e localização do servidor LDAP.

Em particular, a documentação dessas variáveis de ambiente podem ser encontradas em `environment` no arquivo
`docker-compose.yml`.

### KERBEROS

O servidor kerberos tem que estar na porta 88, a library java que usamos para kerberos não gosta de KDC's em outras
portas.

Ou seja, se for usar túnel SSH, deverá usar a porta 88:

``` 
ssh seu_usuario@linux.ime.usp.br -L 88:kerberos:88
```

Algumas configurações de conexão com o kerberos não podem ser definídas por código de aplicação,
então a máquina de execução vai precisar ter um arquivo /etc/krb5.conf

Para desenvolvimento local, a seguinte configuração é suficiente:

``` 
[libdefaults]
	default_realm = LINUX.IME.USP.BR
	
	# por favor seja mais leniente
	ignore_acceptor_hostname = true
	
	# não busque nada no DNS, por favor
	dns_lookup_kdc = no
	dns_lookup_realm = no
	
[realms]
	LINUX.IME.USP.BR = {
	
	# não use UDP, estamos usando túnel SSH, que é TCP
		udp_preference_limit = 1
	}

[domain_realm]
    # o realm do domínio linux.ime.usp.br é o realm LINUX.IME.USP.BR
	linux.ime.usp.br = LINUX.IME.USP.BR
```


- Como eu faço deploy desse projeto?

FIXME: A configuração do docker-compose ainda não está sincronizada com o resto, falta instalar kerberos
no dockerfile, e colocar configuração de krb5.conf no compose.

Um Dockerfile é providenciado para executar o projeto.
A aplicação executa na porta 8080 por padrão, e salva seus dados temporários em `/app/data/database`.

Um arquivo `docker-compose.yml` de conveniência também é providenciado.

