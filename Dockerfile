
# Aqui usamos duas imagens, uma para buildar o projeto, e uma para rodar.
FROM gradle:8-alpine as BUILD_IMAGE

WORKDIR /build
COPY . .

RUN gradle build

FROM amazoncorretto:21-alpine

WORKDIR /app

COPY --from=BUILD_IMAGE /build/build/libs/com.example.backend_jvm-all.jar ./target.jar

CMD ["java", "-jar", "target.jar"]

